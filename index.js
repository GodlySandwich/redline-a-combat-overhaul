/*
/NOTES/

KEYWORDS:
While I left keyword checking enabled, I disabled adding keywords since it adds more than 20 minutes to the patcher's runtime. It's not worth it to add the keywords to 
vanilla/unbalanced modded weapons since the original patch is deleted when the new one is ran, so it's incredibly stupid to add and therefore has been scrapped.
(For reference, the runtime with adding keywords is around 33 minutes. Without adding keywords, it's about a minute.)





*/

//PATCHING STARTS HERE//
registerPatcher({
	info: info,
	gameModes: [xelib.gmTES5, xelib.gmSSE],
	settings: {
		label: 'Skyrim Overhaul Project',
		hide: true,
		templateUrl: `${patcherUrl}/partials/settings.html`,
		defaultSettings: {
			patchFileName: 'skyrimOverhaulPatch.esp'
		}
	},

	requiredFiles: [],
	getFilesToPatch: function(filenames) {
		return filenames;
	},
	execute: (patchFile, helpers, settings, locals) => ({
		process: [/*{
			load: { //Ammunition
				signature: 'AMMO',
				filter: rec => true
			},
			patch: rec => {
				let ContainsKeyword = (xelib.HasKeyword(rec, 'REDAmmoAlreadyPatched'))
				if (ContainsKeyword === false) {
					//Increases weapon damage by five
					let oldDamage = xelib.GetDamage(rec);
					xelib.SetDamage(rec, oldDamage * 5);
					
					//Weight changes are postponed until further notice
					//Reduces ammunition weight by 100
					let oldWeight = xelib.GetWeight(rec);
					xelib.SetWeight(rec, oldWeight / 100);
					
					//Adds keyword to show that the entries are patched
					//xelib.AddKeyword(rec, 'REDAmmoAlreadyPatched');
				}
			}
			
		},*/

		/*{
			load: { //Armor 
				signature: 'ARMO',
				filter: rec => true
			},
			patch: rec => {
				//Weight changes are postponed until further notice
				//Reduces armor weight by a factor of five
				let oldWeight = xelib.GetWeight(rec);
				xelib.SetWeight(rec, oldWeight / 5);
			}
			
		},*/ 

		{
			load: { //Combat Styles
				signature: 'CSTY',
				filter: rec => true
			},
			patch: rec => {
				//Makes everyone more aggresive. 
				xelib.AddElementValue(rec, 'CSGD - General\\Offensive Mult', '0.95');
				xelib.AddElementValue(rec, 'CSGD - General\\Defensive Mult', '0.04');
				//xelib.AddElementValue(rec, 'CSGD - General\\Defensive Mult', `${xelib.GetValue(rec, 'CSGD\\Defensive Mult') / 10}`);
				
				let editorID = xelib.GetValue(rec, 'EDID');
					if (editorID.includes('Melee')) {
						xelib.AddElementValue(rec, 'CSGD\\Equipment Score Mult - Melee', `${xelib.GetValue(rec, 'CSGD\\Equipment Score Mult - Melee') * 1.75}`);
					}
					if (!editorID.includes('Unarmed')) {
						xelib.AddElementValue(rec, 'CSGD\\Equipment Score Mult - Unarmed', '0');
					}
				
				//Keep these disabled until I can figure out what I want to do with these
				/*xelib.AddElementValue(rec, 'CSME - Melee\\Attack Staggered Mult', '9.5');
				xelib.AddElementValue(rec, 'CSME - Melee\\Power Attack Staggered Mult', '9.5');
				xelib.AddElementValue(rec, 'CSME - Melee\\Power Attack Blocking Mult', '9.5');
				xelib.AddElementValue(rec, 'CSME - Melee\\Bash Mult', '9.5');
				xelib.AddElementValue(rec, 'CSME - Melee\\Bash Recoil Mult', '9.5');
				xelib.AddElementValue(rec, 'CSME - Melee\\Bash Attack Mult', '9.5');
				xelib.AddElementValue(rec, 'CSME - Melee\\Bash Power Attack Mult', '9.5');
				xelib.AddElementValue(rec, 'CSME - Melee\\Special Attack Mult', '1.5');*/
			}
			
		},

		{
			load: { //GMST
				signature: 'GMST',
				filter: rec => true
			},
			patch: rec => {
				let editorID = xelib.GetValue(rec, 'EDID');
				if (editorID.includes('fCombatDistance')) {
					xelib.SetValue(rec, 'DATA\\Float', '141.000000');
				}
				
				
			}
			
		},

		{
			load: { //Projectiles
				signature: 'PROJ',
				filter: rec => true
			},
			patch: rec => {
				//If the projectile type is "Arrow," proceed with the patching
				let arrowType = xelib.GetValue(rec, 'DATA\\Type', 'Arrow')
				if (arrowType === 'Arrow') {
					//If the gravity value is greater than 0.15, patch the projectile. Projectiles don't have keywords, so I chose this instead
					let Gravity = xelib.GetElement(rec, 'DATA\\Gravity', '0.15')
					if (Gravity > 0.15) {
						let editorID = xelib.GetValue(rec, 'EDID');
							//If the EditorID contains "Arrow," apply these changes
							if (editorID.includes('Arrow')) {
								xelib.SetFloatValue(rec, 'DATA\\Gravity', `${xelib.GetValue(rec, 'DATA\\Gravity') / 3}`);
								xelib.SetFloatValue(rec, 'DATA\\Speed', `${xelib.GetValue(rec, 'DATA\\Speed') * 2}`);
							}

							//If the EditorID contains "Bolt," apply these changes
							if (editorID.includes('Bolt')) {
								xelib.SetFloatValue(rec, 'DATA\\Gravity', `${xelib.GetValue(rec, 'DATA\\Gravity') / 2.5}`);
								xelib.SetFloatValue(rec, 'DATA\\Speed', `${xelib.GetValue(rec, 'DATA\\Speed') * 1.75}`);
							}
					}
				}
			}
		}, 

		{
			load: { //Races
				signature: 'RACE',
				filter: rec => true
			},
			patch: rec => {
				let ContainsKeyword = (xelib.HasKeyword(rec, 'REDRaceAlreadyPatched'))
				if (ContainsKeyword === false) {
					let raceSize = xelib.GetValue(rec, 'DATA\\Size', 'Size')
					if (raceSize === 'Medium') {
						//Increases the unarmed damage and decreases the reach of all medium-sized races
						//xelib.SetFloatValue(rec, 'DATA\\Unarmed Damage', `${xelib.GetValue(rec, 'DATA\\Unarmed Damage') * 6}`);
						xelib.SetFloatValue(rec, 'DATA\\Unarmed Reach', '84');
					}
					
					if (xelib.HasKeyword(rec, 'ActorTypeNPC')) {
						//Allows any playable race to swim during combat
						xelib.SetFlag(rec, 'DATA\\Flags', 'No Combat In Water', false)
						
						//Changes the strike/attack angle of all playable races
						if (!xelib.HasElement(rec, 'Attacks')) return;
						let attacks = xelib.GetElements(rec, 'Attacks'); // array of the ATKD - Attack Data elements
						attacks.forEach(attack => {
							xelib.SetValue(attack, 'ATKD\\Strike Angle', '50');
						});
					}
					//Adds keyword to show that the entries are patched
					//xelib.AddKeyword(rec, 'REDRaceAlreadyPatched');
				}
			}
		},

		{
			load: { //Weapons
				signature: 'WEAP',
				filter: rec => true
			},
			patch: rec => {	
				// Checks if weapons have the keyword
				//The patcher will ignore a weapon if it already has the keyword
				let ContainsKeyword = (xelib.HasKeyword(rec, 'REDWeaponAlreadyPatched'))
				if (ContainsKeyword === false) {
					//Increases weapon damage by five
					//let oldDamage = xelib.GetDamage(rec);
					//xelib.SetDamage(rec, oldDamage * 5);

					//Weight changes are postponed until further notice
					/*//Reduces weapon weight by a factor of five
					let oldWeight = xelib.GetWeight(rec);
					xelib.SetWeight(rec, oldWeight / 5);*/
					
					//Changes weapon's speeds and reach distances
					//I would turn this into an array and apply it through a for loop, but I like having things readable
					if (xelib.HasKeyword(rec, 'WeapTypeSword')) { //One Handed Swords 
						xelib.SetFloatValue(rec, 'DNAM\\Speed', 1.100000);
						xelib.SetFloatValue(rec, 'DNAM\\Reach', 0.380000);
					}

					if (xelib.HasKeyword(rec, 'WeapTypeMace')) { //One Handed Maces 
						xelib.SetFloatValue(rec, 'DNAM\\Speed', 0.900000);
						xelib.SetFloatValue(rec, 'DNAM\\Reach', 0.280000);
					}

					if (xelib.HasKeyword(rec, 'WeapTypeWarAxe')) { //One Handed Axes 
						xelib.SetFloatValue(rec, 'DNAM\\Speed', 1.000000);
						xelib.SetFloatValue(rec, 'DNAM\\Reach', 0.290000);
					}

					if (xelib.HasKeyword(rec, 'WeapTypeDagger')) { //Daggers 
						xelib.SetFloatValue(rec, 'DNAM\\Speed', 1.350000);
						xelib.SetFloatValue(rec, 'DNAM\\Reach', 0.250000);
					}

					if (xelib.HasKeyword(rec, 'WeapTypeGreatsword')) { //Greatswords 
						xelib.SetFloatValue(rec, 'DNAM\\Speed', 0.850000);
						xelib.SetFloatValue(rec, 'DNAM\\Reach', 1.009000);
					}
					
					if (xelib.HasKeyword(rec, 'WeapTypeWarhammer')) { //Warhammers 
						xelib.SetFloatValue(rec, 'DNAM\\Speed', 0.600000);
						xelib.SetFloatValue(rec, 'DNAM\\Reach', 0.880000);
					}
					
					if (xelib.HasKeyword(rec, 'WeapTypeBattleaxe')) { //Battleaxes 
						xelib.SetFloatValue(rec, 'DNAM\\Speed', 0.666667);
						xelib.SetFloatValue(rec, 'DNAM\\Reach', 0.890000);
					}
					
					/*
					//Experimental Unarmed Edits, Bethesda is weird and only has this weapon equipped during brawls.
					//To-Do: Ask noxcrab about his implementation of unarmed weapons
					let unarmedType = xelib.GetValue(rec, 'DNAM\\Animation Type', 'HandToHandMelee')
					if (unarmedType === 'HandToHandMelee') {//Unarmed
						xelib.SetFloatValue(rec, 'DNAM\\Speed', 1.380000);
						xelib.SetFloatValue(rec, 'DNAM\\Reach', 0.400000);
					}*/
					//xelib.AddKeyword(rec, 'REDWeaponAlreadyPatched');
				}
			}
		}]
	})
});
